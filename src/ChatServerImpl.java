import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Georg Weidel - s0549439
 */
public class ChatServerImpl extends UnicastRemoteObject implements ChatServer {
    private ArrayList<ChatClient> allClients;

    public ChatServerImpl() throws RemoteException{
        allClients = new ArrayList<ChatClient>();
    }

    @Override
    public synchronized boolean addClient(ChatClient objref) throws RemoteException {
        String name = objref.getName();
        for (Iterator<ChatClient> iter = allClients.iterator(); iter.hasNext();){

            ChatClient cc = iter.next();
            try{
                if (cc.getName().equals(name)){
                    return false;
                }
            }catch (RemoteException e){
                iter.remove();
            }
        }
        allClients.add(objref);
        return true;
    }

    @Override
    public synchronized void removeClient(ChatClient objref) throws RemoteException {
        allClients.remove(objref);
    }

    @Override
    public synchronized void sendMessage(String name, String message) throws RemoteException {

        for (Iterator<ChatClient> iter = allClients.iterator(); iter.hasNext();){
            ChatClient cc = iter.next();

            try{
                cc.print(name + ": " + message);
            }catch(RemoteException e){
                iter.remove();
            }
        }
    }

    public static void main(String[] args) {
        try {
            //Erzeugung des Objects
            ChatServerImpl myServer = new ChatServerImpl();

            //Anmeldung der Klasse durch aufruf von static bind beim repository/register
            Naming.rebind("Server", myServer);
            System.out.println("Server started");


        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
