import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Georg Weidel - s0549439
 */
public interface ChatServer extends Remote {
    public boolean addClient(ChatClient objref) throws RemoteException;
    public void removeClient(ChatClient objref) throws RemoteException;
    public void sendMessage(String name, String message) throws RemoteException;
}
