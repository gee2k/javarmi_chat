import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Georg Weidel - s0549439
 */
public interface ChatClient extends Remote {
    public String getName() throws RemoteException;
    public void print(String message) throws RemoteException;
}
