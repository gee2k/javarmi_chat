import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author Georg Weidel - s0549439
 */
public class ChatClientImpl extends UnicastRemoteObject implements ChatClient {

    private String name;

    public ChatClientImpl (String name) throws RemoteException{
        this.name = name;
    }

    @Override
    public synchronized String getName() throws RemoteException {
        return name;
    }

    @Override
    public synchronized void print(String message) throws RemoteException {
        System.out.println(message);
    }

    public static void main(String[] args) {

        InputStreamReader ireader = new InputStreamReader(System.in);
        BufferedReader breader = new BufferedReader(ireader);


        if (args.length < 2){
            System.out.println("Zuwenig Parameter: <Server Name> <Nickname>");
            return;
        }

        //verb aufbauen zum Server
        try{
            System.out.println("Server: " + args[0] + " welcomes: " + args[1]);
            ChatServer myServer = (ChatServer) Naming.lookup("rmi://" + args[0]+ "/Server");
            ChatClient myClient = new ChatClientImpl(args[1]);  //Client mit parameter für nick erstellen

            if (myServer.addClient(myClient)){
                //Client geadded dann...
                System.out.println("connection successful");
                myServer.sendMessage(myClient.getName(), "joined the conversation");  //sende namen und nachricht
                String message = "";

                while (!message.equals("/quit")){
                    message = breader.readLine();
                    myServer.sendMessage(myClient.getName(), message);  //sende namen und nachricht
                }
                System.out.println("bye bye");
                myServer.sendMessage(myClient.getName(), "exits the conversation");
                myServer.removeClient(myClient);
            }
            else{
                System.out.println("This username is allready in use");
            }
        }
        catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
